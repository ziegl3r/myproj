from django.shortcuts import render
from bs4 import BeautifulSoup

def wide(request):
    template = 'sales.html'
    return render(request, template)

def narrow(request):
    template = 'narrow.html'
    return render(request, template)

def order(request):
    template = 'order.html'
    return render(request, template)

def signup(request):
    template = 'signups.html'
    return render(request, template)

def about(request):
    template = "about.html"
    #Scraper code for shiitake feed
    return render(request,template)

def thanks(request):
    template = 'thanks.html'
    return render(request, template)
