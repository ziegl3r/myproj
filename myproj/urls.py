from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'myproj.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'myproj.views.narrow', name='narrow'),
    url(r'^wide/', 'myproj.views.wide', name='wide'),
    url(r'^about/', 'myproj.views.about', name='about'),
    url(r'^thanks/', 'myproj.views.thanks', name='thanks'),
    url(r'^order/', 'myproj.views.order', name='order'),
    url(r'^signup/', 'myproj.views.signup', name='signup'),
)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
            document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
            document_root=settings.MEDIA_ROOT)
